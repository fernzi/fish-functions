function srihash --description 'Calculate subresource integrity hash'
	for url in $argv
		printf 'sha384-%s\n' (curl -s "$url" | openssl dgst -sha384 -binary | openssl enc -base64 -A)
	end
end
