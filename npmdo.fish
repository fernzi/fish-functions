function npmdo --description 'Execute a locally installed node.js binary'
	set -x PATH (npm bin) $PATH
	eval $argv
end
