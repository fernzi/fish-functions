function aurget -d 'Fetch packages from AUR' -a pkg -a dir
	if test -z "$dir"
		git clone "https://aur.archlinux.org/$pkg.git"
	else
		git clone "https://aur.archlinux.org/$pkg.git" "$dir"
	end
end
