function fish_prompt
	set_color EC407A
	printf '%*s\n' $COLUMNS | string replace -a ' ' \u2581
	set_color -o 222 -b EC407A
	printf ' %s ' $USER
	set_color -b F48FB1
	printf ' %s ' (date '+%H:%M')
	set_color normal
	echo ' '
end
