Fish Functions
==============
Some functions for the `Fish shell`_ which make my daily life easier, one way or
another.

.. _Fish shell: http://fishshell.com
