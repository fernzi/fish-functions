function play --description 'Play sound file'
	for fname in $argv
		ffplay -nodisp -autoexit -loglevel quiet $fname
	end > /dev/null
end
