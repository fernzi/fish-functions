function find-up -d 'Search for files in reverse' -w find
	set -l fpath $argv[1]
	while test $fpath != /
		find $fpath -maxdepth 1 -mindepth 1 $argv[2..-1]
		set fpath (readlink -f $fpath/..)
	end
end
