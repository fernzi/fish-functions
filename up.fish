function up --description 'Go up N directories' --argument N
	set -l p
	set -l n $argv[1]
	if test -z $n
		set n 1
	end
	for i in (seq $n)
		set p "$p../"
	end
	if test -n "$p"
		cd $p
	end
end
