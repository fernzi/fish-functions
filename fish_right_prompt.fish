function fish_right_prompt
	set_color -o 222 -b F48FB1
	set -l cdir (basename $PWD)
	if test $PWD = $HOME
		echo ' ~ '
	else if test (string length $cdir) -gt 15
		printf ' %.15s… ' $cdir
	else
		printf ' %s ' $cdir
	end
	set_color normal
end
