function venvdo --description 'Execute a command within the closest Python virtualenv'
	# FIXME: Better way of finding venv
	set fpath $PWD
	while test $fpath != /
		if test -d $fpath/.venv
			set venvpath $fpath/.venv
			break
		end
		set fpath (dirname $fpath)
	end
	if test -n "$venvpath"
		# source $venvpath/bin/activate.fish
		# eval $argv
		# deactivate
		set -x PATH $venvpath/bin $PATH
		eval $argv
	else
		echo 'Virtualenv not found!'
	end
end
